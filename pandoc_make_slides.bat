pandoc -s -S -i -t dzslides --mathjax ML_for_Hackers___Supervised_Learning.md -o ML_for_Hackers_Supervised_Learning_dzslides.html
pandoc -s --self-contained --webtex -i -t s5 ML_for_Hackers___Supervised_Learning.md -o ML_for_Hackers_Supervised_Learning_s5.html
pandoc --webtex -i -t slidy  ML_for_Hackers___Supervised_Learning.md -o ML_for_Hackers_Supervised_Learning_slidy.html
pandoc -s --mathjax -i -t slideous  ML_for_Hackers___Supervised_Learning.md -o ML_for_Hackers_Supervised_Learning_slideous.html

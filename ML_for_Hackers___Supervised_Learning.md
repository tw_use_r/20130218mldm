% ML for Hackers II -- Supervised Learning
% c3h3
% 2013/02/18





# 本次導讀的章節

- 第三章 Classification：在第三章中，主要使用 e-mail spam 作為範例，來介紹一下相關的工具與套件！首先，在 cleaning data 的部分，使用了 R 中的 tm 套件；接著，再運用 tm 模組幫助我們抽取出 TF-IDF 等資訊，來進行 Naive Bays Classifier 的訓練與測試！

- 第五章 Regression：第五章的內容大致上可以分成三個部分：一開始，直接使用 y 的平均數，作為 y=f(x) 的估計值；其次，加入 x 進行估計之後，能夠提升多少估計值與解釋力？最後，說明一下當 x 之中具有「類別變數」時，要如何求取迴歸係數？


# 各章節使用的 Dataset

- 第三章：[spamassassin](http://spamassassin.apache.org/)

<img src="arrowlogo.png" width="300"/>



# 第五章：吸菸資料


```r
ages <- read.csv(file.path("data", "longevity.csv"))
head(ages)
```

```
##   Smokes AgeAtDeath
## 1      1         75
## 2      1         72
## 3      1         66
## 4      1         74
## 5      1         69
## 6      1         65
```


# 第五章：吸菸資料


```r
tail(ages)
```

```
##      Smokes AgeAtDeath
## 995       0         77
## 996       0         83
## 997       0         78
## 998       0         79
## 999       0         71
## 1000      0         81
```



# 第五章：吸菸資料


```r
ggplot(ages, aes(x = AgeAtDeath, fill = factor(Smokes))) + geom_density() + 
    facet_grid(Smokes ~ .)
```

![Smoking Data](figure/smoke_data_graph .png) 



# 第五章：身高-體重-性別資料


```r
heights.weights <- read.csv("data/01_heights_weights_genders.csv", header = TRUE, 
    sep = ",")
ggplot(heights.weights, aes(x = Weight, y = Height, color = Gender)) + geom_point() + 
    geom_smooth(method = "lm")
```

![plot of chunk HWG_Data](figure/HWG_Data.png) 


# 第五章：網站流量資料


```r
top.1000.sites <- read.csv("data/top_1000_sites.tsv", sep = "\t", stringsAsFactors = FALSE)
head(top.1000.sites)
```

```
##   Rank          Site                     Category UniqueVisitors Reach
## 1    1  facebook.com              Social Networks      880000000  47.2
## 2    2   youtube.com                 Online Video      800000000  42.7
## 3    3     yahoo.com                  Web Portals      660000000  35.3
## 4    4      live.com               Search Engines      550000000  29.3
## 5    5 wikipedia.org Dictionaries & Encyclopedias      490000000  26.2
## 6    6       msn.com                  Web Portals      450000000  24.0
##   PageViews HasAdvertising InEnglish TLD
## 1   9.1e+11            Yes       Yes com
## 2   1.0e+11            Yes       Yes com
## 3   7.7e+10            Yes       Yes com
## 4   3.6e+10            Yes       Yes com
## 5   7.0e+09             No       Yes org
## 6   1.5e+10            Yes       Yes com
```



# 第五章：網站流量資料


```r
ggplot(top.1000.sites, aes(x = log(PageViews), y = log(UniqueVisitors))) + geom_point()
```

![plot of chunk PageViewsData_graph](figure/PageViewsData_graph.png) 


# 所使用的 Tools

- ggplot2 
- tm (第三章)
- lm (第五章)

# 回到第三章的分類問題...

- 首先，讓我們來看看「作者用來 cleaning data 的工具」 ......
- 接著，讓我們來看看「Data 的樣子」......
- 然後，再來看看「如何使用tm 工具」 ......
- 最後，就是「如何建造 Naive Baysian Classifier？」......
- 接下來，就是「看 Codes 學寫 Codes」的時間囉！

# 作者用來 cleaning data 的工具 (get.msg)


```r
get.msg <- function(path) {
    con <- file(path, open = "rt", encoding = "latin1")
    text <- readLines(con)
    # The message always begins after the first full line break
    msg <- text[seq(which(text == "")[1] + 1, length(text), 1)]
    close(con)
    return(paste(msg, collapse = "\n"))
}
```


# E-mail Data 的樣子


```r
library("tm")
# Set the global paths
spam.path <- file.path("data", "spam")
spam.docs <- dir(spam.path)
spam.docs <- spam.docs[which(spam.docs != "cmds")]
all.spam <- sapply(head(spam.docs), function(p) get.msg(file.path(spam.path, 
    p)))
```

```
## Warning: invalid input found on input connection
## 'data/spam/00006.5ab5620d3d7c6c0db76234556a16f6c1'
```

```r
print(all.spam[1])
```

```
##                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    00001.7848dde101aa985090474a91ec93fcf0 
## "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">\n<HTML><HEAD>\n<META content=3D\"text/html; charset=3Dwindows-1252\" http-equiv=3DContent-T=\nype>\n<META content=3D\"MSHTML 5.00.2314.1000\" name=3DGENERATOR></HEAD>\n<BODY><!-- Inserted by Calypso -->\n<TABLE border=3D0 cellPadding=3D0 cellSpacing=3D2 id=3D_CalyPrintHeader_ r=\nules=3Dnone \nstyle=3D\"COLOR: black; DISPLAY: none\" width=3D\"100%\">\n  <TBODY>\n  <TR>\n    <TD colSpan=3D3>\n      <HR color=3Dblack noShade SIZE=3D1>\n    </TD></TR></TD></TR>\n  <TR>\n    <TD colSpan=3D3>\n      <HR color=3Dblack noShade SIZE=3D1>\n    </TD></TR></TBODY></TABLE><!-- End Calypso --><!-- Inserted by Calypso=\n --><FONT \ncolor=3D#000000 face=3DVERDANA,ARIAL,HELVETICA size=3D-2><BR></FONT></TD><=\n/TR></TABLE><!-- End Calypso --><FONT color=3D#ff0000 \nface=3D\"Copperplate Gothic Bold\" size=3D5 PTSIZE=3D\"10\">\n<CENTER>Save up to 70% on Life Insurance.</CENTER></FONT><FONT color=3D#ff=\n0000 \nface=3D\"Copperplate Gothic Bold\" size=3D5 PTSIZE=3D\"10\">\n<CENTER>Why Spend More Than You Have To?\n<CENTER><FONT color=3D#ff0000 face=3D\"Copperplate Gothic Bold\" size=3D5 PT=\nSIZE=3D\"10\">\n<CENTER>Life Quote Savings\n<CENTER>\n<P align=3Dleft></P>\n<P align=3Dleft></P></FONT></U></I></B><BR></FONT></U></B></U></I>\n<P></P>\n<CENTER>\n<TABLE border=3D0 borderColor=3D#111111 cellPadding=3D0 cellSpacing=3D0 wi=\ndth=3D650>\n  <TBODY></TBODY></TABLE>\n<TABLE border=3D0 borderColor=3D#111111 cellPadding=3D5 cellSpacing=3D0 wi=\ndth=3D650>\n  <TBODY>\n  <TR>\n    <TD colSpan=3D2 width=3D\"35%\"><B><FONT face=3DVerdana size=3D4>Ensurin=\ng your \n      family's financial security is very important. Life Quote Savings ma=\nkes \n      buying life insurance simple and affordable. We Provide FREE Access =\nto The \n      Very Best Companies and The Lowest Rates.</FONT></B></TD></TR>\n  <TR>\n    <TD align=3Dmiddle vAlign=3Dtop width=3D\"18%\">\n      <TABLE borderColor=3D#111111 width=3D\"100%\">\n        <TBODY>\n        <TR>\n          <TD style=3D\"PADDING-LEFT: 5px; PADDING-RIGHT: 5px\" width=3D\"100=\n%\"><FONT \n            face=3DVerdana size=3D4><B>Life Quote Savings</B> is FAST, EAS=\nY and \n            SAVES you money! Let us help you get started with the best val=\nues in \n            the country on new coverage. You can SAVE hundreds or even tho=\nusands \n            of dollars by requesting a FREE quote from Lifequote Savings. =\nOur \n            service will take you less than 5 minutes to complete. Shop an=\nd \n            compare. SAVE up to 70% on all types of Life insurance! \n</FONT></TD></TR>\n        <TR><BR><BR>\n          <TD height=3D50 style=3D\"PADDING-LEFT: 5px; PADDING-RIGHT: 5px\" \n          width=3D\"100%\">\n            <P align=3Dcenter><B><FONT face=3DVerdana size=3D5><A \n            href=3D\"http://website.e365.cc/savequote/\">Click Here For Your=\n \n            Free Quote!</A></FONT></B></P></TD>\n          <P><FONT face=3DVerdana size=3D4><STRONG>\n          <CENTER>Protecting your family is the best investment you'll eve=\nr \n          make!<BR></B></TD></TR>\n        <TR><BR><BR></STRONG></FONT></TD></TR></TD></TR>\n        <TR></TR></TBODY></TABLE>\n      <P align=3Dleft><FONT face=3D\"Arial, Helvetica, sans-serif\" size=3D2=\n></FONT></P>\n      <P></P>\n      <CENTER><BR><BR><BR>\n      <P></P>\n      <P align=3Dleft><BR></B><BR><BR><BR><BR></P>\n      <P align=3Dcenter><BR></P>\n      <P align=3Dleft><BR></B><BR><BR></FONT>If you are in receipt of this=\n email \n      in error and/or wish to be removed from our list, <A \n      href=3D\"mailto:coins@btamail.net.cn\">PLEASE CLICK HERE</A> AND TYPE =\nREMOVE. If you \n      reside in any state which prohibits e-mail solicitations for insuran=\nce, \n      please disregard this \n      email.<BR></FONT><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR=\n><BR><BR><BR></FONT></P></CENTER></CENTER></TR></TBODY></TABLE></CENTER></=\nCENTER></CENTER></CENTER></CENTER></BODY></HTML>\n\n\n"
```



# E-mail Data 的樣子


```r
print(all.spam[5])
```

```
##                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           00005.57696a39d7d84318ce497886896bf90d 
## "I thought you might like these:\n1) Slim Down - Guaranteed to lose 10-12 lbs in 30 days\nhttp://www.freeyankee.com/cgi/fy2/to.cgi?l=822slim1\n\n2) Fight The Risk of Cancer! \nhttp://www.freeyankee.com/cgi/fy2/to.cgi?l=822nic1 \n\n3) Get the Child Support You Deserve - Free Legal Advice \nhttp://www.freeyankee.com/cgi/fy2/to.cgi?l=822ppl1\n\nOffer Manager\nDaily-Deals\n\n\n\n\n\n\n\n\nIf you wish to leave this list please use the link below.\nhttp://www.qves.com/trim/?social@linux.ie%7C29%7C134077\n\n\n-- \nIrish Linux Users' Group Social Events: social@linux.ie\nhttp://www.linux.ie/mailman/listinfo/social for (un)subscription information.\nList maintainer: listmaster@linux.ie\n"
```


# 作者用來 cleaning data 的工具 (get.tdm)


```r
get.tdm <- function(doc.vec) {
    control <- list(stopwords = TRUE, removePunctuation = TRUE, removeNumbers = TRUE, 
        minDocFreq = 2)
    doc.corpus <- Corpus(VectorSource(doc.vec))
    doc.dtm <- TermDocumentMatrix(doc.corpus, control)
    return(doc.dtm)
}
```


# 如何使用 tm 中的工具 (Corpus / TermDocumentMatrix)

```r
spam.tdm <- get.tdm(all.spam)
print(spam.tdm)
```

```
## A term-document matrix (349 terms, 6 documents)
## 
## Non-/sparse entries: 434/1660
## Sparsity           : 79%
## Maximal term length: 44 
## Weighting          : term frequency (tf)
```


# 如何使用 tm 中的工具 (Corpus / TermDocumentMatrix)

```r
str(spam.tdm)
```

```
## List of 6
##  $ i       : int [1:434] 2 10 13 14 15 16 17 18 19 20 ...
##  $ j       : int [1:434] 1 1 1 1 1 1 1 1 1 1 ...
##  $ v       : num [1:434] 1 1 1 1 1 1 1 1 1 1 ...
##  $ nrow    : int 349
##  $ ncol    : int 6
##  $ dimnames:List of 2
##   ..$ Terms: chr [1:349] "absolutely" "access" "account" "adult" ...
##   ..$ Docs : chr [1:6] "00001.7848dde101aa985090474a91ec93fcf0" "00002.d94f1b97e48ed3b553b3508d116e6a09" "00003.2ee33bc6eacdb11f38d052c44819ba6c" "00004.eac8de8d759b7e74154f142194282724" ...
##  - attr(*, "class")= chr [1:2] "TermDocumentMatrix" "simple_triplet_matrix"
##  - attr(*, "Weighting")= chr [1:2] "term frequency" "tf"
```



# 運用 TDM 來進行分析與 Training

```r
spam.matrix <- as.matrix(spam.tdm)
spam.counts <- rowSums(spam.matrix)
head(spam.counts)
```

```
##    absolutely        access       account         adult        adults 
##             2             4             1             5             1 
## advertisement 
##             1
```


# 運用 TDM 來進行分析與 Training

```r
spam.df <- data.frame(cbind(names(spam.counts), as.numeric(spam.counts)), stringsAsFactors = FALSE)
names(spam.df) <- c("term", "frequency")
spam.df$frequency <- as.numeric(spam.df$frequency)
head(spam.df)
```

```
##            term frequency
## 1    absolutely         2
## 2        access         4
## 3       account         1
## 4         adult         5
## 5        adults         1
## 6 advertisement         1
```


# 複習一下 TF-IDE 的定義：

- TF 的定義 $$\mathrm{tf_{i,j}} = \frac{n_{i,j}}{\sum_k n_{k,j}}$$
- IDF 的定義 $$\mathrm{idf_{i}} =  \log \frac{|D|}{|\{j: t_{i} \in d_{j}\}|}$$
- [Wikipedia Reference](http://zh.wikipedia.org/wiki/TF-IDF)


# 運用 TDM 來計算 TF-IDF

```r
spam.occurrence <- sapply(1:nrow(spam.matrix), function(i) {
    length(which(spam.matrix[i, ] > 0))/ncol(spam.matrix)
})
spam.density <- spam.df$frequency/sum(spam.df$frequency)
```


# 完成 Spam 的訓練 Data Frame

```r
# Add the term density and occurrence rate
spam.df <- transform(spam.df, density = spam.density, occurrence = spam.occurrence)
head(spam.df)
```

```
##            term frequency  density occurrence
## 1    absolutely         2 0.003503     0.1667
## 2        access         4 0.007005     0.3333
## 3       account         1 0.001751     0.1667
## 4         adult         5 0.008757     0.1667
## 5        adults         1 0.001751     0.1667
## 6 advertisement         1 0.001751     0.1667
```


# 運用類似步驟，可完成 Ham 的訓練 Data Frame

```r
easyham.path <- file.path("data", "easy_ham")
# Now do the same for the EASY HAM email
easyham.docs <- dir(easyham.path)
easyham.docs <- easyham.docs[which(easyham.docs != "cmds")]
all.easyham <- sapply(head(easyham.docs), function(p) get.msg(file.path(easyham.path, 
    p)))

easyham.tdm <- get.tdm(all.easyham)

easyham.matrix <- as.matrix(easyham.tdm)
easyham.counts <- rowSums(easyham.matrix)
easyham.df <- data.frame(cbind(names(easyham.counts), as.numeric(easyham.counts)), 
    stringsAsFactors = FALSE)
names(easyham.df) <- c("term", "frequency")
easyham.df$frequency <- as.numeric(easyham.df$frequency)
easyham.occurrence <- sapply(1:nrow(easyham.matrix), function(i) {
    length(which(easyham.matrix[i, ] > 0))/ncol(easyham.matrix)
})
easyham.density <- easyham.df$frequency/sum(easyham.df$frequency)

easyham.df <- transform(easyham.df, density = easyham.density, occurrence = easyham.occurrence)
```


# 如何建造 Naive Baysian Classifier？

- 先分別建造好 Ham 和 Spam 的 Training Data Frame
- 接著運用 Training Data Frame 來計算新的 E-mail 是 Ham or Spam 的機率?

# Review: Naive Baysian Classifier

<img src="naive_bayes_graph.png" width="300"/>
    
# Review: Naive Baysian Classifier

- 已知： $$P(w_j|C)$$

- 已知： $$P(C)$$

- 欲知： $$P(C|w_1,w2,\ldots,w_n)$$

- Baysian： $$P(C|w_1,w2,\ldots,w_n) \propto \prod_j P(w_j|C) \times P(C)$$

- 先分別建造好 Ham 和 Spam 的 Training Data Frame
- 接著運用 Training Data Frame 來計算新的 E-mail 是 Ham or Spam 的機率?



# Naive Baysian Classifier for E-mail SPAM


```r
classify.email <- function(path, training.df, prior = 0.5, c = 1e-06) {
    # Here, we use many of the support functions to get the email text data in
    # a workable format
    msg <- get.msg(path)
    msg.tdm <- get.tdm(msg)
    msg.freq <- rowSums(as.matrix(msg.tdm))
    # Find intersections of words
    msg.match <- intersect(names(msg.freq), training.df$term)
    # Now, we just perform the naive Bayes calculation
    if (length(msg.match) < 1) {
        return(prior * c^(length(msg.freq)))
    } else {
        match.probs <- training.df$occurrence[match(msg.match, training.df$term)]
        return(prior * prod(match.probs) * c^(length(msg.freq) - length(msg.match)))
    }
}
```



# 如何運用 Naive Baysian Classifier

- 就請大家自己回家看一下作者的 Sample Codes 囉！


# Regression Analysis

- 未完待續，下回分曉！

